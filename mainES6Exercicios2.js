const usuarios = [
    { nome: 'rafael', idade: 23, empresa: 'Novah' },
    { nome: 'liliane', idade: 15, empresa: 'Unitronica' },
    { nome: 'arnaldo', idade: 30, empresa: 'Google' },
   ];
 
   
// filtando array por seguimentos 
const idade = usuarios.map(users => users.idade);
const nome = usuarios.map(names => names.nome);
const empresa = usuarios.map(empresas => empresas.empresa)


console.log(nome);
console.log(idade);
console.log(empresa);

//trabalha na novah e tem mais de 18 anos
const encontraFuncionario = usuarios.filter(users => users.empresa === "Novah" && users.idade >=18);
console.log(encontraFuncionario);

//encontra funcionarios menores de idade
const menoresIdade = usuarios.filter(users => users.idade < 18);
console.log(menoresIdade);
console.log('menores de idade acima');

//encontra funcionarios maiores de idade
const maioresIdade = usuarios.filter(users => users.idade >18);
console.log(maioresIdade);
console.log("acima maqiores de idade");

//utilizando find procure por alguem que trabalha no google
const trabalhaGoogle = usuarios.find(users => users.empresa === 'Google');
console.log(trabalhaGoogle);

//multiplicar por 2 e reparar que tem acima de 50 anos
const idosos = usuarios.map(users => ({...users, idade : users.idade * 2})).filter(users => users.idade <=50);
console.log(idosos);

