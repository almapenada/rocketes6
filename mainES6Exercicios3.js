//converta as funcões a seguir em funções arrow
//3.1 retorna numero +10
const arr = [1, 2, 3, 4, 5];
arr.map(function(item) {
 return item + 10;
});

//console.log(arr);

const array = [1, 2, 3, 4, 5];
const elemento = array.map(elementos => elementos+10);

//console.log(elemento);
//retorna numero  * 2

const dados = [2, 5, 6, 8, 91];

const dado = dados.map(info => info*2);

//console.log(dado);
// 3.2
// Dica: Utilize uma constante pra function
const usuario = { nome: 'rafael', idade: 31 };
function mostraIdade(usuario) {
 return usuario.idade;
}
mostraIdade(usuario);
//console.log(mostraIdade(usuario));

const aluno = { nome: 'jorjao', idade: 31 };

const idadeAluno = aluno => aluno.idade;

//console.log(idadeAluno(aluno));

//3.3
// Dica: Utilize uma constante pra function
const nome = "Rafael";
const idade = 32;

function mostraUsuario(nome = 'pedrao', idade = 18) {
 return { nome, idade };
}

console.log(mostraUsuario(nome, idade));
//3.3 arroy function

const login = 'almapenada';
const age = 32;

const viewerUser = (nome = 'pedrao', idade = 18) => ({ nome, idade});

console.log(viewerUser(nome, idade));

// 3.4
const promise = function() {
    return new Promise(function(resolve, reject) {
    return resolve();
    })
   }
//arrow function segue linha analize
const promise = () => new Promise(resolve,reject => resolve());