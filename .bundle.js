"use strict";

var nome = ' rafael';
var idade = 32;
var usuario = {
  nome: nome,
  idade: idade,
  cidade: 'Garibaldi'
};
console.log(usuario); //arrow compactação das informações

var name = 'rafael';
var age = 32;
var login = {
  name: name,
  age: age,
  city: ' Garibaldi'
};
console.log(login);
