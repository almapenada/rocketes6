//4.1 exercicios
//resestrturação simples
const empresa = {
    nome: 'Unitronica',
    endereco: {
        cidade: 'Garibaldi',
        estado: 'RS',
    }
   };
   
const { nome, endereco: {cidade, estado}} = empresa;


console.log(cidade);

const dadosConta = {
    login: 'almapenada',
    senha: 1234,
    endereco:{
        rua: 'pedrocattani',
        city: 'Garibaldi',
        numero: 400,
    }
}

const {login, senha, endereco: { rua, city, numero}} = dadosConta;

console.log(login);
console.log(senha);
console.log(rua);
console.log(city);
console.log(numero);

//4.2 Desestruturacao em parametros
function mostraInfo(usuario) {
    return `${usuario.nome} tem ${usuario.idade} anos.`;
   }
   mostraInfo({ nome: 'Diego', idade: 23 })
   
function mostraAge({nome, idade}) {{
    return `${nome} tem ${idade} anos.`;
  
}
    
}

mostraAge({ nome: 'rafael', idade: 32});