class Usuario {
    constructor(email, password){
        this.email = email;
        this.password = password;
    }
    isAdmin(){
        return this.admin === true;
    }
}

class Admin extends Usuario {
    constructor(){
        super();
        this.admin = true;
    }
}

const User1 = new Usuario('rafael@unitronica.com.br', '123');
const Admin1 = new Admin('rafael@unitronica.com.br', '123');

console.log(User1.isAdmin());
console.log(Admin1.isAdmin());

class User {
    constructor(nome, senha){
            this.name = nome;
            this.senha = senha;
        }
        hasAcesses(){
            return this.acesses === true;
    }
}

class Acesses extends User{
    constructor(){
        super();
        this.acesses = true;
    }
}

const Rafael = new Acesses('rafael', '123');
const Lili = new User('liliane', '1234');

console.log(Lili.hasAcesses());

var passar = Lili.hasAcesses();
console.log(passar);


if (passar === true) {
    console.log('tem acesso');
}else{
    console.log('não tem acesso');
}

