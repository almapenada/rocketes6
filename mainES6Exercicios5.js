//5.1
//atibuir para valor x o primeiro valor do elemento e depois disso tudo para y

const arr = [1, 2, 3, 4, 5];

console.log(arr);

const [x, ...y] = arr;
//Crie uma fucao que receve inumeros parametros e retorna a soma de todos eles 
//função para somar todos elementos recebidor
function soma(...params){
    return params.reduce((a, b) => a + b);
}

console.log(soma(1, 2, 4, 5, 6));//18

//5.2 A partir do objeto e utilizando o operador spred
const usuario = {
    nome: 'Rafael',
    idade: 23,
    endereco: {
        cidade: 'Garibaldi',
        estado: 'RS',
    }
};

console.log(usuario);

const usuario2 = {...usuario, nome: 'jorjao'};

const usuario3 = {...usuario, nome: 'arnaldo'};

console.log(usuario2);
console.log(usuario3);

const dados ={
    login: 'almapenada',
    senha: 123,
    endereco: {
        rua: 'antonio debiasi',
        city: 'garibaldi',
        numero: 400,
    }
};

const dados2 = {...dados, login: 'pedao'};
console.log(dados2);


