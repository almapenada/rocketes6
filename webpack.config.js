module.exports = {
  entry: './main.js',
  output: {
    path: __dirname,
    filename: 'bundle,js',
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exlude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ],
  },
};